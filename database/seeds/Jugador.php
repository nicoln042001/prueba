<?php

use Illuminate\Database\Seeder;
use App\Models\JugadorModel;

class Jugador extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jugador = array(
            [
                'jugador' => 'Jugador 1',
            ],
            [
                'jugador' => 'Jugador 2',
              
            ]
        );

        foreach ($jugador as $jug)
        {
            $jugador = new JugadorModel();
            $jugador->jugador = $jug['jugador'];
            $jugador->save();
        }

    }
}
