<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartidaModel extends Model
{
    protected $table = 'partida_models';
    protected $primaryKey = ['id'];
    protected $guarded = ['id'];

    public function JugadorPartida(){
        return $this->belongsTo('App\Models\jugador_partida');
    }
}
