<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JugadorModel extends Model
{
    protected $table = 'jugador_models';
    protected $primaryKey = ['id'];
    protected $guarded = ['id'];
    protected $fillable = ['jugador'];

    public function JugadorPartida(){
        return $this->belongsTo('App\Models\jugador_partida');
    }
}
