<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class jugador_partida extends Model
{
    protected $table = 'jugador_partidas';
    protected $primaryKey = ['id'];
    protected $guarded = ['id'];
    protected $fillable = ['idJugador','idPartida'];

    public function Partida(){
        return $this->HasMany('App\Models\PartidaModel','idPartida','id');
    }

    public function Jugador(){
        return $this->hasMany('App\Models\JugadorModel', 'idJugador','id');
    }
}
