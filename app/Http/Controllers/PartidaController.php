<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\jugador_partida;
use App\Models\JugadorModel;
use App\Models\PartidaModel;

class PartidaController extends Controller
{
    //Carga la vista principal de la partida
    public function index()
    {
        return view('/partida');
    }
//Crea la nueba partida y el jugador
    public function nuevaPartida(Request $request)
    {
      $partida =  new PartidaModel([]);
       $jugador = new JugadorModel([
        'jugador' => $request->get('jugador')
       ]);
        $partida->save();
        $jugador->save();
        return view('/partida');
    }

    //inicia la partida e inserta el jugador con su respectiva partida relacionada
    public function iniciarpartida(Request $request, $partida,$jugador)
    {
        $partida = PartidaModel::find($partida);
        $jugador = JugadorModel::find($jugador);

        $jugadorpartida = new jugador_partida([
            'idJugador' => $request->get($jugador),
            'idPartida' => $request->get($partida)

        ]);
    return view('/partida',compact($partida,$jugador,$jugadorpartida));
    }

    //Se une a la partida existente y crea un nuevo jugador en la base de dstos
    public function UnirsePartida(Request $request, $partida)
    {
        $partida = PartidaModel::find($partida);
        JugadorModel::create([
            'jugador'=>$request->get('jugador2')
        ]);
        return view('/partida', compact($partida));
    }
}
