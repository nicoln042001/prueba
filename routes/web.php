<?php

use App\Models\PartidaModel;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('partida', 'PartidaController');
Route::get('partida/index', 'PartidaController@index');
Route::get('/iniciarpartida', 'PartidaController@iniciarpartida');
Route::post('/nuevaPartida', 'PartidaController@nuevaPartida');
Route::post('/UnirsePartida', 'PartidaController@UnirsePartida');