$(document).ready(function(){
    //Mostrar la caja de texto para unirse a una partida
    $('#unirse').click(function(){
        $('#idPartida').css('display','block');   
    });

    $('#nueva').click(function(){
        $('#nombre').css('display','block');
    });
});

//Datos principales del juego, tablero, nombres, marcas, mensaje
let tablero = ["", "", "", "", "", "", "", "", ""]
let contador = 1
let nombre1 ="jugador 1"
let nombre2 = "jugador 2"
let marca1 = "x"
let marca2 = "O"
let trikiEstado = false
let empateEstado = false
let mensaje = document.getElementById('mensaje')

//Verificar en la matriz las posiciones las cuales dan triki

const verificarTriki = (marca, jugador) => {
    if ((tablero[0] == marca && tablero[1] == marca && tablero[2] == marca) || (tablero[0] == marca && tablero[3] == marca && tablero[6] == marca) || (tablero[0] == marca && tablero[4] == marca && tablero[8] == marca)) {
        trikiEstado = true
        mensaje.innerHTML = "El ganador es " + jugador
    }
    if (tablero[1] == marca  && tablero[4] == marca  && tablero[7] == marca){
        trikiEstado = true
        mensaje.innerHTML = "El ganador es " + jugador
    }
    if ((tablero[2] == marca  && tablero[5] == marca  && tablero[8] == marca ) || (tablero[2] == marca  && tablero[4] == marca  && tablero[6] == marca)) {
        trikiEstado = true
        mensaje.innerHTML = "El ganador es " + jugador
    }
    if (tablero[3] == marca  && tablero[4] == marca  && tablero[5] == marca) {
        trikiEstado = true
        mensaje.innerHTML = "El ganador es " + jugador

    }
    if (tablero[6] == marca  && tablero[7] == marca  && tablero[8] == marca){
        trikiEstado = true
        mensaje.alert = "El ganador es " + jugador
    
    }
    
    if (tablero[0] != "" && tablero[1] != "" && tablero[2] != "" && tablero[3] != "" && tablero[4] != "" && tablero[5] != "" && tablero[6] != "" && tablero[7] != "" && tablero[8] != "" && trikiEstado == false){
        empateEstado = true
        mensaje.innerHTML = "¡Ha habido empate!"            
    
    }


}

//Permite marcar el turno del jugador

const  marcar = (id) => {
    let idEntero = parseInt(id.id)
    if (contador % 2 != 0) {
        let marca = marca1
        tablero[idEntero - 1] = marca
        id.innerHTML = marca
        var element = document.getElementById(id.id);
        element.classList.add('disabled');
        verificarTriki(marca, nombre1)

    }
    else {
        let marca = marca2
        tablero[idEntero - 1] = marca
        id.innerHTML = marca
        verificarTriki(marca, nombre2)
        console.log(empateEstado)
        console.log(trikiEstado)
        var element = document.getElementById(id.id);
        element.classList.add('disabled');
    }

    contador++

    //Maneja el turno de cada jugador

    if (contador % 2 != 0 && !trikiEstado && !empateEstado) {
        mensaje.innerHTML = "Turno jugador " + nombre1
    }
    else {
        if (contador < 10 && !trikiEstado && !empateEstado) {
            mensaje.innerHTML = "Turno jugador " + nombre2
        }
    }

   
}

