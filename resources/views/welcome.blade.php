<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/global.css">
    <title>TIC TAC TOE</title>
</head>

<body>
    <form action="/nuevaPartida" method="post">
        <div class="container row text-center mt-3">
            <div class="col-md-12">
                <a id="nueva" class="btn btn-primary" href="#">Nueva partida</a>
                @csrf
                <div class="row" id="nombre">
                    <div class="col-md-6 m-auto">
                        <span class="mt-3">Nombre</span>
                        <input type="text" class="form-control m-3" name="jugador">
                        <button class="btn btn-success">Entrar</button>
                    </div>
                </div>
            </div>
    </form>
    <form action="/UnirsePartida" method="post">
        <div class="col-md-12 mt-3">
            <a class="btn btn-primary" href="#" id="unirse">Unirse a partida</a>
        </div>
        <div class="col-md-12 text-center row" id="idPartida">
            <div class="col-md-6 m-auto">

                <span>Nombre</span>
                <input type="text" class="form-control" name="jugador2">
            </div>
            <div class="col-md-6 m-auto">
                <span>No. Partida</span>
                <input type="text" class="form-control">
                <button class="btn btn-success mt-3">Entrar</button>

            </div>
        </div>
        </div>
    </form>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../js/global.js"></script>

</html>