<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/global.css">
    <title>Nueva Partida</title>
</head>
<body>
    <div class="container mt-5">
        <div class="col-md-12 row">
            <div class="col-md-3">
                <span>No. Partida</span>
                <input type="number" class="form-control">
            </div>
            <div class="col-md-3">
            <span>Nombre Jugador 1</span>
                <input type="text" class="form-control" name="jugador1">
            </div>
            <div class="col-md-3">
            <span>Nombre Jugador 2</span>
                <input type="text" class="form-control" name="jugador2">
            </div>
            <div class="col-md-3" id="mensaje">Turno jugador 1</div>

        </div>
        <div class="col-md-4 row text-center m-auto mt-5">
            <div id="1" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro1"></div>
            <div id="2" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro2"></div>
            <div id="3" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro3"></div>
            <div id="4" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro4"></div>
            <div id="5" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro5"></div>
            <div id="6" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro6"></div>
            <div id="7" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro7"></div>
            <div id="8" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro8"></div>
            <div id="9" class="col-4 btn btn-info border border-white cuadro" onclick="marcar(this)" name="cuadro9"></div>
        </div>
    </div>
</body>
<script src="http://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script type="text/javascript" src="../../js/global.js"></script>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</html>
